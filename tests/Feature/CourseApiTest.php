<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CourseApiTest extends TestCase
{
    use RefreshDatabase, DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this
        ->call('POST', '/api/register', [
            'email' => 'test@gmail.com',
            'name' => 'test',
            'password' => 'test1234567',
            'password_confirmation' => 'test1234567'
        ]);
        $response->assertStatus(201);


        $response = $this
        ->call('POST', '/api/login', [
            'email' => 'test@gmail.com',
            'password' => 'test1234567',
        ]);
        $response->assertStatus(200);
        $token = $response['token'];
        $result = $this->withHeaders(['Authorization' => "Bearer {$token}"])
        ->call('POST', '/api/course', [
            'title' => 'test title',
            'subtitle' => 'test subtitle',
            'abstract' => 'abstract',
            'overview' => 'overview',
            'introVideo' => 'introVideo',
            'curriculum' => [
                'eligibility' => 'eligibility',
                'pre_requisites' => 'pre_requisites'
            ]
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $id = $result['data']['id'];
        $response->assertStatus(200);

        $response = $this
        ->call('GET', '/api/course/'.$id, [
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $response->assertStatus(202);

        $response = $this
        ->call('GET', '/api/courses', [
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $response->assertStatus(202);

        $response = $this
        ->call('PUT', '/api/course/'.$id, [
            'title' => 'test title',
            'subtitle' => 'test subtitle',
            'abstract' => 'abstract',
            'overview' => 'overview',
            'introVideo' => 'introVideo',
            'curriculum' => [
                'eligibility' => 'test',
                'pre_requisites' => 'pre_requisites'
            ]
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $response->assertStatus(202);

        $response = $this
        ->call('DELETE', '/api/course/'.$id, [
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $response->assertStatus(202);
        
    }
}
