<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class UserApiTest extends TestCase
{
    use RefreshDatabase, DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserApi()
    {
        $response = $this
        ->call('POST', '/api/register', [
            'email' => 'test@gmail.com',
            'name' => 'test',
            'password' => 'test1234567',
            'password_confirmation' => 'test1234567'
        ]);
        $response->assertStatus(201);
        $response = $this
        ->call('POST', '/api/login', [
            'email' => 'test@gmail.com',
            'password' => 'test1234567',
        ]);
        $response->assertStatus(200);
        $token = $response['token'];
        $response = $this
        ->call('GET', '/api/user', [
        ],[],[],['HTTP_Authorization' => "Bearer {$token}"]);
        $response->assertStatus(200);
    }
}
