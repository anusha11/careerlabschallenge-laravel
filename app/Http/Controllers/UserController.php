<?php

    namespace App\Http\Controllers;

    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use JWTAuth;
    use Tymon\JWTAuth\Exceptions\JWTException;
        /**
 * @OA\Info(title="Swagger API Documentation", version="0.1")
 */

 /**
  * @OAS\SecurityScheme(
  *     securityScheme="bearerAuth",
  *     type="http",
  *     scheme="bearer"
  * )
 **/
    class UserController extends Controller
    {
        /**
        * @OA\Post(
        *  path="/api/login",
        * tags={"User"},
        *  summary="Login",
        *  operationId="login",
      
        *  @OA\Parameter(
        *      name="email",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="password",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),    
        *  @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=401,
        *      description="Unauthorized"
        *  ),
        *  @OA\Response(
        *      response=500,
        *      description="Could not create token"
        *  ),
        *  @OA\Response(
        *      response=404,
        *      description="not found"
        *  ),
        *    )
         */

        public function authenticate(Request $request)
        {
            $credentials = $request->only('email', 'password');

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            return response()->json(compact('token'));
        }
        

        /**
        * @OA\Post(
        *  path="/api/register",
        * tags={"User"},
        *  summary="Register",
        *  operationId="register",
      
        *  @OA\Parameter(
        *      name="name",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="email",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),    
         *  @OA\Parameter(
        *      name="password",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="password_confirmation",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),    
        *  @OA\Response(
        *      response=201,
        *      description="user created",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=400,
        *      description="Validation error"
        *  ),
        * )
        */

        public function register(Request $request)
        
        {
                $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'),201);
        }

        /**
        * @OA\Get(
        *  path="/api/user",
        *  tags={"User"},
        *  summary="Get authenticated user detail",
        *  operationId="Get authenticated user detail",
        *  security={
        *         {"bearer": {}}
        *     },      
        *  @OA\Response(
        *      response=200,
        *      description="succes",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=404,
        *      description="user not found"
        *  ),
        * )
        */
        public function getAuthenticatedUser(Request $request)
            {

                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                            return response()->json(['token_expired'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                            return response()->json(['token_invalid'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                            return response()->json(['token_absent'], $e->getStatusCode());

                    }

                    return response()->json(compact('user'));
            }
    }