<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

/**
  * @OAS\SecurityScheme(
  *     securityScheme="bearerAuth",
  *     type="http",
  *     scheme="bearer"
  * )
 **/

class CourseController extends Controller
{
    /**
        * @OA\Post(
        *  path="/api/course",
        * tags={"Course"},
        *  summary="Create a course with curriculum object containing eligibility and pre_requisites",
        *  operationId="create",
        *  security={
        *         {"bearer": {}}
        *     },   
      
        *  @OA\Parameter(
        *      name="title",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="subtitle",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ), 
         *  @OA\Parameter(
        *      name="overview",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="abstract",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="introVideo",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),   
        
        *  @OA\Parameter(
        *      name="curriculum",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="object",
        *          default="""curriculum"": {""eligibility"": ""<eligibility>"",""pre_requisites"": ""<pre_requisites>""}"
        *      )
        *  ),   
        
        *  @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=401,
        *      description="Unauthorized"
        *  ),
        *  @OA\Response(
        *      response=400,
        *      description="Validation error"
        *  ),
        *  @OA\Response(
        *      response=404,
        *      description="not found"
        *  ),
        *    )
         */

    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:200',
            'subtitle' => 'required|string|max:500',
            'abstract' => 'required|string|max:600',
            'overview' => 'required|string|max:800',
            'introVideo' => 'required|string|max:200',
            'curriculum.eligibility' => 'required|string|max:800',
            'curriculum.pre_requisites' => 'required|string|max:800',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $curricullum_data = array('id'=>(string) Str::uuid(),'eligibility'=>$request['curriculum']['eligibility'],
        "pre_requisites"=>$request['curriculum']['pre_requisites']
        );
        
        DB::table('curriculum')->insert($curricullum_data);
        $cur_data = DB::table('curriculum')->latest()->first();

        $data=array('id'=>(string) Str::uuid(),'title'=>$request->get('title'),"subtitle"=>$request->get('subtitle'),
        "abstract"=>$request->get('abstract'),"overview"=>$request->get('overview'),'introVideo' => $request->get('introVideo'),
        "curriculum_id" => $cur_data->id);
        DB::table('course')->insert($data);
        $data = DB::table('course')->latest()->first();
        return response()->json(['data' => $data], 200);

    }

     /**
        * @OA\Get(
        *  path="/api/courses",
        *  tags={"Course"},
        *  summary="Get course count along with courses",
        *  operationId="get",
        *  security={
        *         {"bearer": {}}
        *     },   
      
        
        *  @OA\Response(
        *      response=200,
        *      description="Success",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=401,
        *      description="Unauthorized"
        *  ),
        *  @OA\Response(
        *      response=404,
        *      description="not found"
        *  ),
        *    )
         */

   
    public function getCourses(Request $request){
        $data = DB::table('course')->get();
        $count = DB::table('course')->count();
        return response()->json(['count' => $count, 'data' => $data], 202);
    }


    /**
        * @OA\Get(
        *  path="/api/course/{id}",
        * tags={"Course"},
        *  summary="Get a course record",
        *  operationId="create",
        *  security={
        *         {"bearer": {}}
        *     },   

        *  @OA\Parameter(
        *     name="id",
        *     in="path",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),  

        *  @OA\Response(
        *      response=202,
        *      description="Success",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=400,
        *      description="Invalid id passed"
        *  ),
        *)
    */
    public function getCourse($id){
        $data = DB::table('course')->where('id', $id)->first();
        if($data){
            return response()->json(['data' => $data], 202);
        }
        else{
            return response()->json(['data' => 'Invalid id passed'], 400);
        }
       
    }


   /**
        * @OA\Delete(
        *  path="/api/course/{id}",
        * tags={"Course"},
        *  summary="Delete a course record",
        *  operationId="delete",
        *  security={
        *         {"bearer": {}}
        *     },   

        *  @OA\Parameter(
        *     name="id",
        *     in="path",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),  

        *  @OA\Response(
        *      response=202,
        *      description="Record deleted successfully",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=400,
        *      description="Invalid id passed"
        *  ),
        *)
    */
    public function delete($id){
        $data = DB::table('course')->where('id', $id)->first();
        if($data){
            DB::table('course')->where('id', $id)->delete();
            return response()->json(['data' => 'record deleted successfully'], 202);
        }
        return response()->json(['data' => 'Invalid id passed'], 400);
    }

    /**
        * @OA\Put(
        *  path="/api/course/{id}",
        * tags={"Course"},
        *  summary="Update a course record",
        *  operationId="update",
        *  security={
        *         {"bearer": {}}
        *     },   

        *  @OA\Parameter(
        *     name="id",
        *     in="path",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),  

        *  @OA\Parameter(
        *      name="title",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="subtitle",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="abstract",
        *      in="query",
        *     required=true,
        *      @OA\Schema(
        *      type="string"
        *      )
        *  ),    
        *  @OA\Parameter(
        *      name="overview",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),   
        *  @OA\Parameter(
        *      name="introVideo",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="string"
        *      )
        *  ),  
         *  @OA\Parameter(
        *      name="curriculum",
        *      in="query",
        *      required=true,
        *      @OA\Schema(
        *          type="object",
        *      default="""curriculum"": {""eligibility"": ""<eligibility>"",""pre_requisites"": ""<pre_requisites>""}"    
        *      )
        *  ),     

        *  @OA\Response(
        *      response=202,
        *      description="Success",
        *      @OA\MediaType(
        *          mediaType="application/json",
        *      )
        *  ),
        *  @OA\Response(
        *      response=400,
        *      description="Invalid id passed"
        *  ),
        *)
    */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:200',
            'subtitle' => 'required|string|max:500',
            'abstract' => 'required|string|max:600',
            'overview' => 'required|string|max:800',
            'introVideo' => 'required|string|max:200',
            'curriculum.eligibility' => 'required|string|max:800',
            'curriculum.pre_requisites' => 'required|string|max:800',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $data = DB::table('course')->where('id', $id)->first();
        $curricullum_data = array('id'=>(string) Str::uuid(),'eligibility'=>$request['curriculum']['eligibility'],
        "pre_requisites"=>$request['curriculum']['pre_requisites']
        );
        if($data){
            $curriculum_id = $data->curriculum_id;
            DB::table('curriculum')->where('id', $curriculum_id)->update($curricullum_data);

            $data=array('title'=>$request->get('title'),"subtitle"=>$request->get('subtitle'),
            "abstract"=>$request->get('abstract'),"overview"=>$request->get('overview'),'introVideo' => $request->get('introVideo'),
            "curriculum_id" => $curriculum_id);

            DB::table('course')->where('id', $id)->update($data);
            return response()->json(['data' => 'record updated successfully'], 202);
        }
        return response()->json(['data' => 'Invalid id passed'], 400);
    }
}
